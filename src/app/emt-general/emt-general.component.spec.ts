import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmtGeneralComponent } from './emt-general.component';

describe('EmtGeneralComponent', () => {
  let component: EmtGeneralComponent;
  let fixture: ComponentFixture<EmtGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmtGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmtGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
