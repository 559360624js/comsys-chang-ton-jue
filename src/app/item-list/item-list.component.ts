import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  displayedColumns_item_list: string[] = ['position', 'name', 'name_customer', 'weight', 'symbol','many'];
  dataSource_item_list = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource_item_list.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  position: number;
  name_customer: string;
  weight: number;
  symbol: string;
  many: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'ข้าวผัดปู', name_customer: 'สุชาติ' , weight: 60 , symbol: 'อาหารคาว', many: 1 },
  {position: 2, name: 'พะแนง', name_customer: 'สุเทพ' , weight: 50, symbol: 'อาหารคาว', many: 1 },
  {position: 3, name: 'ผัดไทยกุ้งสด', name_customer: 'สุขุม' , weight: 80, symbol: 'อาหารคาว', many: 1 },
  {position: 4, name: 'หลนปู', name_customer: 'สุขำ' , weight: 100, symbol: 'อาหารคาว', many: 1 },

];
