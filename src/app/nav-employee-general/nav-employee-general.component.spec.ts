import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavEmployeeGeneralComponent } from './nav-employee-general.component';

describe('NavEmployeeGeneralComponent', () => {
  let component: NavEmployeeGeneralComponent;
  let fixture: ComponentFixture<NavEmployeeGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavEmployeeGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavEmployeeGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
