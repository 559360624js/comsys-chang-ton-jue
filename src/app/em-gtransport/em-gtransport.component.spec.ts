import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmGTransportComponent } from './em-gtransport.component';

describe('EmGTransportComponent', () => {
  let component: EmGTransportComponent;
  let fixture: ComponentFixture<EmGTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmGTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmGTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
