import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-shop-customer',
  templateUrl: './shop-customer.component.html',
  styleUrls: ['./shop-customer.component.scss']
})
export class ShopCustomerComponent implements OnInit {

  displayedColumns_shop_customer: string[] = ['position', 'name', 'weight', 'symbol','many'];
  dataSource_shop_customer = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource_shop_customer.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  many: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'ข้าวผัดปู' , weight: 60 , symbol: 'อาหารคาว', many: 1 },
  {position: 2, name: 'พะแนง', weight: 50, symbol: 'อาหารคาว', many: 1 },
  {position: 3, name: 'ผัดไทยกุ้งสด', weight: 80, symbol: 'อาหารคาว', many: 1 },
  {position: 4, name: 'หลนปู', weight: 100, symbol: 'อาหารคาว', many: 1 },

];
