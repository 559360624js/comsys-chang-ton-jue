import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  displayedColumns_employee : string[] = ['position', 'name', 'rank', 'money', 'symbol'];
  dataSource_employee = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource_employee.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  position: number;
  rank: string;
  money: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'ต้น', rank: 'ผู้จัดการ' , money: 30000, symbol: 'พิษณุโลก'},
  {position: 2, name: 'ช้าง', rank: 'ประธาน', money: 50000, symbol: 'พิษณุโลก'},
  {position: 3, name: 'จือ', rank: 'ผู้จัดการ', money: 30000, symbol: 'พิษณุโลก'},
  {position: 4, name: 'หลน', rank: 'พนักงาน', money: 15000, symbol: 'พิษณุโลก'},
  {position: 5, name: 'ข้าว', rank: 'พนักงาน', money: 15000, symbol: 'พิษณุโลก'},


];
