import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavEmployeeTransportComponent } from './nav-employee-transport.component';

describe('NavEmployeeTransportComponent', () => {
  let component: NavEmployeeTransportComponent;
  let fixture: ComponentFixture<NavEmployeeTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavEmployeeTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavEmployeeTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
