import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmGGeneralComponent } from './em-ggeneral.component';

describe('EmGGeneralComponent', () => {
  let component: EmGGeneralComponent;
  let fixture: ComponentFixture<EmGGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmGGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmGGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
