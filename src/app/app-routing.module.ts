import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShopsComponent } from './shops/shops.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { RegistorComponent } from './registor/registor.component';
import { AdminComponent } from './admin/admin.component';
import { EmployeeComponent } from './employee/employee.component';
import { CustomerComponent } from './customer/customer.component';
import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CompanyComponent } from './company/company.component';
import { ShopCustomerComponent } from './shop-customer/shop-customer.component';
import { ProfileComponent } from './profile/profile.component';
import { ShopLoginComponent } from './shop-login/shop-login.component';
import { EmGGeneralComponent } from './em-ggeneral/em-ggeneral.component';
import { EmGItemComponent } from './em-gitem/em-gitem.component';
import { EmGTransportComponent } from './em-gtransport/em-gtransport.component';
import { EmtGeneralComponent } from './emt-general/emt-general.component';
import { EmtTransportComponent } from './emt-transport/emt-transport.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'store', component: ShopsComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registor', component: RegistorComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'customer', component: CustomerComponent },
  { path: 'item', component: ItemComponent },
  { path: 'item-list', component: ItemListComponent },
  { path: 'customer-list', component: CustomerListComponent },
  { path: 'company', component: CompanyComponent },
  { path: 'shop-customer', component: ShopCustomerComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'shop-login', component: ShopLoginComponent },
  { path: 'em-gitem', component: EmGItemComponent },
  { path: 'em-gtarnsport', component: EmGTransportComponent },
  { path: 'em-ggeneral', component: EmGGeneralComponent },
  { path: 'emt-general', component: EmtGeneralComponent },
  { path: 'emt-tarnsport', component: EmtTransportComponent },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
