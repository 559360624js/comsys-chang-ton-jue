import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatTabsModule} from '@angular/material/tabs';
import {MatMenuModule} from '@angular/material/menu';


@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatCardModule , MatTableModule , MatPaginatorModule , MatTabsModule , MatMenuModule],
  exports: [MatButtonModule, MatCheckboxModule, MatCardModule , MatTableModule , MatPaginatorModule , MatTabsModule , MatMenuModule],
})
export class MaterialModule { }
