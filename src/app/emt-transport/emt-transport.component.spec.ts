import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmtTransportComponent } from './emt-transport.component';

describe('EmtTransportComponent', () => {
  let component: EmtTransportComponent;
  let fixture: ComponentFixture<EmtTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmtTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmtTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
