import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-emt-transport',
  templateUrl: './emt-transport.component.html',
  styleUrls: ['./emt-transport.component.scss']
})
export class EmtTransportComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'name_cus', 'weight', 'many', 'address', 'company'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  name_cus: string;
  position: number;
  weight: number;
  many: number;
  company: string;
  address: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'ข้าวผัดปู', name_cus: 'สุชาติ', weight: 60 , many: 1, address: '1/2 หมู่ 3', company: 'พิษณุโลก'},
  {position: 2, name: 'พะแนง', name_cus: 'สุชาติ', weight: 50, many: 1, address: '1/2 หมู่ 3',  company: 'พิษณุโลก'},
  {position: 3, name: 'ผัดไทยกุ้งสด',name_cus: 'สุชาติ', weight: 80, many: 1, address: '1/2 หมู่ 3', company: 'พิษณุโลก'},


];
