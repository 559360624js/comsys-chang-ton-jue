import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { RegistorComponent } from './registor/registor.component';
import { AdminComponent } from './admin/admin.component';
import { ShopsComponent } from './shops/shops.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { EmployeeComponent } from './employee/employee.component';
import { CustomerComponent } from './customer/customer.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemComponent } from './item/item.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CompanyComponent } from './company/company.component';
import { ShopCustomerComponent } from './shop-customer/shop-customer.component';
import { NavAdminComponent } from './nav-admin/nav-admin.component';
import { NavCustomerComponent } from './nav-customer/nav-customer.component';
import { NavLoginComponent } from './nav-login/nav-login.component';
import { ProfileComponent } from './profile/profile.component';
import { ShopLoginComponent } from './shop-login/shop-login.component';
import { NavEmployeeGeneralComponent } from './nav-employee-general/nav-employee-general.component';
import { NavEmployeeTransportComponent } from './nav-employee-transport/nav-employee-transport.component';
import { EmGGeneralComponent } from './em-ggeneral/em-ggeneral.component';
import { EmGItemComponent } from './em-gitem/em-gitem.component';
import { EmGTransportComponent } from './em-gtransport/em-gtransport.component';
import { EmtGeneralComponent } from './emt-general/emt-general.component';
import { EmtTransportComponent } from './emt-transport/emt-transport.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    ContactComponent,
    LoginComponent,
    RegistorComponent,
    AdminComponent,
    ShopsComponent,
    EmployeeComponent,
    CustomerComponent,
    ItemListComponent,
    ItemComponent,
    CustomerListComponent,
    CompanyComponent,
    ShopCustomerComponent,
    NavAdminComponent,
    NavCustomerComponent,
    NavLoginComponent,
    ProfileComponent,
    ShopLoginComponent,
    NavEmployeeGeneralComponent,
    NavEmployeeTransportComponent,
    EmGGeneralComponent,
    EmGItemComponent,
    EmGTransportComponent,
    EmtGeneralComponent,
    EmtTransportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
