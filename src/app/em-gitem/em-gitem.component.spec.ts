import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmGItemComponent } from './em-gitem.component';

describe('EmGItemComponent', () => {
  let component: EmGItemComponent;
  let fixture: ComponentFixture<EmGItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmGItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmGItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
