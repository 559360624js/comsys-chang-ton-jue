import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-em-gitem',
  templateUrl: './em-gitem.component.html',
  styleUrls: ['./em-gitem.component.scss']
})
export class EmGItemComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'address', 'company'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  company: string;
  address: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'ข้าวผัดปู', weight: 60 , symbol: 'อาหารคาว', address: '33/7 ม.2', company: 'พิษณุโลก'},
  {position: 2, name: 'พะแนง', weight: 50, symbol: 'อาหารคาว',address: '33/7 ม.2',  company: 'พิษณุโลก'},
  {position: 3, name: 'ผัดไทยกุ้งสด', weight: 80, symbol: 'อาหารคาว',address: '33/7 ม.2',  company: 'พิษณุโลก'},
  {position: 4, name: 'หลนปู', weight: 100, symbol: 'อาหารคาว', address: '33/7 ม.2',  company: 'พิษณุโลก'},
  {position: 5, name: 'ข้าวซอย', weight: 60, symbol: 'อาหารคาว', address: '33/7 ม.2',  company: 'พิษณุโลก'},
  {position: 6, name: 'เขียวหวานไก่', weight: 60, symbol: 'อาหารคาว',address: '33/7 ม.2',   company: 'พิษณุโลก'},
  {position: 7, name: 'กะเพรา', weight: 40, symbol: 'อาหารคาว',address: '33/7 ม.2',   company: 'พิษณุโลก'},
  {position: 8, name: 'ไข่ตุ๋น', weight: 40, symbol: 'อาหารคาว',address: '33/7 ม.2',  company: 'พิษณุโลก'},
  {position: 9, name: 'ก๋วยเตี๋ยวต้มยำ', weight: 40, symbol: 'อาหารคาว',address: '33/7 ม.2',  company: 'พิษณุโลก'},

];
