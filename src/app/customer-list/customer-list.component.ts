import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  displayedColumns_customer_list: string[] = ['id_customer', 'name_customer', 'age','sex'];
  dataSource_customer_list = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource_customer_list.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  id_customer: number;
  name_customer: string;
  age: number;
  sex: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id_customer: 1,  name_customer: 'สุชาติ' , age: 30 , sex: 'ชาย' },
  {id_customer: 2,  name_customer: 'สุเทพ' , age: 50, sex: 'ชาย' },
  {id_customer: 3, name_customer: 'สุขุม' , age: 28, sex: 'ชาย' },
  {id_customer: 4,  name_customer: 'สุขำ' , age: 32, sex: 'หญิง' },

];
