import { Component, OnInit ,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSource1 = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA1);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'ข้าวผัดปู', weight: 60 , symbol: 'กำลังจัดส่ง'},
  {position: 2, name: 'พะแนง', weight: 50, symbol: 'กำลังจัดส่ง'},
  {position: 3, name: 'ผัดไทยกุ้งสด', weight: 80, symbol: 'กำลังจัดส่ง'},
  {position: 4, name: 'หลนปู', weight: 100, symbol: 'กำลังจัดส่ง'},
  {position: 5, name: 'ข้าวซอย', weight: 60, symbol: 'กำลังจัดส่ง'},
  {position: 6, name: 'เขียวหวานไก่', weight: 60, symbol: 'กำลังจัดส่ง'},
  {position: 7, name: 'กะเพรา', weight: 40, symbol: 'กำลังจัดส่ง'},
  {position: 8, name: 'ไข่ตุ๋น', weight: 40, symbol: 'กำลังจัดส่ง'},
  {position: 9, name: 'ก๋วยเตี๋ยวต้มยำ', weight: 40, symbol: 'กำลังจัดส่ง'},

];

const ELEMENT_DATA1: PeriodicElement[] = [
  {position: 1, name: 'ข้าวผัดปู', weight: 60 , symbol: 'ส่งเสร็จแล้ว'},
  {position: 2, name: 'พะแนง', weight: 50, symbol: 'ส่งเสร็จแล้ว'},
  {position: 3, name: 'ผัดไทยกุ้งสด', weight: 80, symbol: 'ส่งเสร็จแล้ว'},
  {position: 4, name: 'หลนปู', weight: 100, symbol: 'ส่งเสร็จแล้ว'},
  {position: 5, name: 'ข้าวซอย', weight: 60, symbol: 'ส่งเสร็จแล้ว'},
  {position: 6, name: 'เขียวหวานไก่', weight: 60, symbol: 'ส่งเสร็จแล้ว'},
  {position: 7, name: 'กะเพรา', weight: 40, symbol: 'ส่งเสร็จแล้ว'},
  {position: 8, name: 'ไข่ตุ๋น', weight: 40, symbol: 'ส่งเสร็จแล้ว'},
  {position: 9, name: 'ก๋วยเตี๋ยวต้มยำ', weight: 40, symbol: 'ส่งเสร็จแล้ว'},

];
